#!/usr/bin/python3

#    Copyright © 2017-2021 IceGuye.
#
#    This program is free software: you can redistribute it and/or
#    modify it under the terms of the GNU General Public License as
#    published by the Free Software Foundation, version 3 of the
#    License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see
#    <http://www.gnu.org/licenses/>.

import math
from time import time
from statistics import mean
from direct.showbase.ShowBase import ShowBase
from direct.showbase.BufferViewer import BufferViewer
from panda3d.core import *
from panda3d.bullet import *
from direct.actor.Actor import Actor
import simplepbr
import player
import archery
import gui
import os
from loadgame import load_data
import jdatetime
from inventory import inventory
from addonhandler import load_addons
import handevents

loadPrcFileData('', '''
bullet-filter-algorithm groups-mask
view-frustum-cull false
framebuffer-multisample true
multisamples 2
default-antialias-enable true
''')

def set_win_props():
    global win_x_o
    global win_y_o
    global props
    global fullscreen_status
    props.set_size(win_x_o, win_y_o)
    props.setTitle("Project J")
    props.set_fullscreen(fullscreen_status)
    base.win.requestProperties(props)

def toggle_fullscreen():
    global fullscreen_status
    if fullscreen_status == False:
        fullscreen_status = True
    else:
        fullscreen_status = False
    set_win_props()
    
def walk_task(task):
    dt = globalClock.get_dt()
    stop_walking = True
    walk_angle = 0
    walk_spd_x = 0
    walk_spd_y = 0
    is_down = base.mouseWatcherNode.is_button_down
    if is_down(forward_button):
        walk_angle = 0
    if is_down(backward_button):
        walk_angle = 180
    if is_down(left_button):
        walk_angle = 270
    if is_down(right_button):
        walk_angle = 90
    if is_down(forward_button) and is_down(left_button):
        walk_angle = 315
    if is_down(forward_button) and is_down(right_button):
        walk_angle = 45
    if is_down(backward_button) and is_down(left_button):
        walk_angle = 225
    if is_down(backward_button) and is_down(right_button):
        walk_angle = 135
    walk_spd_x = -walk_speed * math.sin(math.radians(walk_angle))
    walk_spd_y = -walk_speed * math.cos(math.radians(walk_angle))
    stop_walking = is_down(forward_button) == False and is_down(backward_button) == False and is_down(left_button) == False and is_down(right_button) == False
    if stop_walking:
        ply_nd.setLinearMovement(Vec3(0, 0, 0), True)
    else:
        ply_bp_np.set_h(cam_anchor.get_h())
        ply_nd.setLinearMovement(Vec3(walk_spd_x, walk_spd_y, 0), True)
    return task.cont

def cam_task(task):
    global win_x_o
    global win_y_o
    max_dist = 8
    set_dist = 8
    cam_anchor.set_fluid_pos(ply_bp_np.get_pos())
    cam_anchor.set_fluid_z(ply_bp_np.get_z() + 0.85)
    cam_anchor.set_fluid_x(ply_bp_np.get_x())
    cam_dist_start.set_fluid_pos(cam_anchor.get_pos())
    cam_dist_start.set_fluid_x(cam_anchor, -0.25)
    cam_dist_end.set_fluid_pos(cam_anchor.get_pos())
    cam_dist_end.set_fluid_x(cam_anchor, -0.25)
    cam_dist_end.set_fluid_y(cam_anchor, 9999)
    pfrom = TransformState.makePos(cam_dist_start.get_pos())
    pto = TransformState.makePos(cam_dist_end.get_pos())
    shape = BulletSphereShape(0.15)
    penetration = 0.05
    hitresult = world.sweep_test_closest(shape, pfrom, pto, BitMask32.bit(0), penetration)
    hitpos = hitresult.get_hit_pos()
    hitnode = hitresult.get_node()
    has_hit = hitresult.has_hit()
    if has_hit:
        pow_x = math.pow((hitpos.get_x() - cam_dist_start.get_x()), 2)
        pow_y = math.pow((hitpos.get_y() - cam_dist_start.get_y()), 2)
        pow_z = math.pow((hitpos.get_z() - cam_dist_start.get_z()), 2)
        set_dist = math.sqrt(pow_x + pow_y + pow_z)
    if set_dist > max_dist:
        set_dist = max_dist
    camera.set_fluid_pos(cam_anchor.get_pos())
    camera.set_fluid_y(cam_anchor, set_dist)
    camera.set_fluid_x(cam_anchor, -0.25)
    camera.set_h(cam_anchor.get_h() + 180)
    camera.set_p(-cam_anchor.get_p())

    win_x = base.win.getXSize()
    win_y = base.win.getYSize()
    if win_x != win_x_o or win_y != win_y_o:
        win_ratio = win_x / win_y
        film_size_x = 24 * win_ratio
        lens = base.cam.node().get_lens()
        lens.set_film_size(film_size_x, 24)
        base.cam.node().set_lens(lens)
        win_x_o = win_x
        win_y_o = win_y

    return task.cont

def physics_task(task):
    dt = globalClock.get_dt()
    world.do_physics(dt)
    return task.cont

def mouse_task(task):
    mw = base.mouseWatcherNode
    has_mouse = mw.has_mouse()
    current_mode = props.get_mouse_mode()
    if has_mouse:
        if current_mode == WindowProperties.M_relative:
            x = mw.get_mouse_x()
            y = mw.get_mouse_y()
            cam_anchor.set_h(-x * 30)
            cam_anchor.set_p(-y * 30)
    return task.cont

def skybox_task(task):
    skybox.set_pos(camera.get_pos())
    return task.cont

def sun_task(task):
    global sun_set_color
    dt = globalClock.get_dt()
    # Sun calculation
    hours = jdatetime.game_epoch_hours
    minutes = jdatetime.game_epoch_minutes
    seconds = jdatetime.game_epoch_seconds
    hours = hours + minutes / 60 + seconds / 3600
    civil_twilight_end = 18.5
    civil_twilight_start = 5.5
    sunset = civil_twilight_end - 0.5
    sunrise = civil_twilight_start + 0.5
    #sun_lux = 80000
    abs_to_midday = abs(hours - 12)
    sun_lux = 100000 - (100000/6 * abs_to_midday)
    sun_anchor.set_fluid_pos(camera.get_pos())
    sun_np.set_fluid_pos(sun_anchor.get_pos())
    sun_np.set_fluid_z(sun_anchor, 3400)
    sun_anchor.set_hpr(0, 0, hours * (-15) + 180)
    sun_np.lookAt(sun_anchor)

    if hours <= sunset-0.5 and hours >= sunrise+0.5:
        if sun_lux < 30000:
            sun_lux = 30000
        sun.set_color(sun_lux)
    
    elif hours > sunset-0.5 and hours < civil_twilight_end:
        if sun_lux < 30000:
            sun_lux = 30000
        sunset_factor = civil_twilight_end - hours
        if sunset_factor < 0:
            sunset_factor = 0
        sunrise_factor = 1 - sunset_factor
        colortemp_deduction = 3600 * (1 - sunset_factor)
        colortemp = 6500 - colortemp_deduction
        sun.setColorTemperature(colortemp)
        sun.set_color(sun.get_color() * sun_lux * sunset_factor)
    elif hours > civil_twilight_start and hours < sunrise+0.5:
        if sun_lux < 30000:
            sun_lux = 30000
        sunrise_factor = (civil_twilight_start + 1) - hours
        if sunrise_factor < 0:
            sunrise_factor = 0
        sunset_factor = 1 - sunrise_factor
        colortemp_deduction = 3600 * sunrise_factor
        colortemp = 6500 - colortemp_deduction
        sun.setColorTemperature(colortemp)
        sun.set_color(sun.get_color() * sun_lux * (1-sunrise_factor))
    else:
        if sun_lux < 30000:
            sun_lux = 30000
        sunset_factor = 0
        sunrise_factor = 1 - sunset_factor
        sun.set_color(sun.get_color() * sun_lux * (1-sunrise_factor))
    mask = BitMask32.bit(0)
    p_from = sun_np.get_pos()
    p_to = sun_anchor.get_pos()
    sun_ray = world.ray_test_closest(p_from, p_to, mask)
    
    if hours > sunset or hours < sunrise:
        if sun_ray.has_hit():
            if sun_set_color > 0:
                sun_set_color = sun_set_color - dt
            else:
                sun_set_color = 0
        else:
            if sun_set_color < sunset_factor:
                sun_set_color = sun_set_color + dt
            else:
                sun_set_color = sunset_factor 
        sun.set_color(sun.get_color() * sun_set_color)

    # Ambient light calculation
    if alight_triggered == False:
        if hours >= sunrise and hours <= sunset:
            alight_lux = 20000 - 3266 * abs_to_midday
            alight.set_color((alight_lux, alight_lux, alight_lux, alight_lux))
        elif hours > sunset and hours <= civil_twilight_end:
            alight_lux = (civil_twilight_end - hours) * 799.996
            alight.set_color((alight_lux, alight_lux, alight_lux, alight_lux))
        elif hours < sunrise and hours >= civil_twilight_start:
            alight_lux = (hours - civil_twilight_start) * 799.996
            alight.set_color((alight_lux, alight_lux, alight_lux, alight_lux))
        else:
            alight.set_color((0.002, 0.002, 0.002, 0.002))

    return task.cont

def eye_exposure(task):
    global pupil_dia
    global rhodopsin
    dt = globalClock.get_dt()
    jdt = dt * 20
    sun_pos = sun_np.get_pos()
    sun_anchor_pos = sun_anchor.get_pos()
    mask = BitMask32.bit(0)
    sun_ray = world.ray_test_closest(sun_pos, sun_anchor_pos, mask)
    sun_vec = sun_pos - sun_anchor_pos
    sun_vec.normalize()
    cam_pos = camera.get_pos()
    cam_anchor_pos = cam_anchor.get_pos()
    cam_vec = cam_anchor_pos - cam_pos
    cam_vec.normalize()
    sun_cam_angle = sun_vec.angle_deg(cam_vec)
    alight_color = alight.get_color()
    alight_lux = alight_color.get_x()
    pupil_per_sec = 0.006 / 60
    pupil_per_lux = 0.006 / 20000
    pupil_dia_target = 0.008 - pupil_per_lux * alight_lux
    if sun_cam_angle < 16 and sun_ray.has_hit() == False:
        pupil_per_deg = 0.003 / 16
        pupil_by_sun = pupil_per_deg * (16 - sun_cam_angle)
        pupil_dia_target = pupil_dia_target - pupil_by_sun
        if pupil_dia_target < 0.0016:
            pupil_dia_target = 0.0016
    if pupil_dia - pupil_dia_target < -0.0003:
        pupil_dia = pupil_dia + (pupil_per_sec * jdt)
    elif pupil_dia - pupil_dia_target >0.0003:
        pupil_dia = pupil_dia - (pupil_per_sec * jdt)
    pupil_size = (pupil_dia / 2) ** 2 * math.pi

    mid_night_rho_exp = 110000
    noon_rho_exp = 3.3
    
    rho_per_sec = (math.log(mid_night_rho_exp) - math.log(noon_rho_exp)) / 1200
    rhodopsin_target = math.log(noon_rho_exp)
    twilight_margin_top = noon_rho_exp * 20000 * (((0.002 / 2) ** 2) * 3.14159265) * 20000
    twilight_margin_bottom = mid_night_rho_exp * (((0.00788 / 2) ** 2) * 3.14159265) * 0.002
    if pupil_size * alight_lux * rhodopsin_target > twilight_margin_top:
        rhodopsin_target = math.log(twilight_margin_top / (pupil_size * alight_lux))
    elif pupil_size * alight_lux * rhodopsin_target < twilight_margin_bottom:
        rhodopsin_target = math.log(twilight_margin_bottom / (pupil_size * alight_lux))

    if sun_cam_angle < 16 and sun_ray.has_hit() == False:
        rho_rate = 0.8 / 16
        rho_by_sun = rho_rate * (16 - sun_cam_angle)
        rhodopsin_target = rhodopsin_target - rho_by_sun
    if rhodopsin - rhodopsin_target < -(rho_per_sec*2):
        rhodopsin = rhodopsin + rho_per_sec * jdt
    elif rhodopsin - rhodopsin_target > rho_per_sec*2:
        rhodopsin = rhodopsin - rho_per_sec * jdt
    pipeline.exposure = pupil_size * math.exp(rhodopsin)
    
    return task.cont

def combat_type_switch(button):
    if button == "0":
        player.player["combat type"] = None
    elif button == "1":
        player.player["combat type"] = "archery"
    elif button == "2":
        player.player["combat type"] = "melee"

def draw_bow(task):
    global draw_sec
    global arrow_loading_time
    dt = globalClock.get_dt()
    if len(inventory["Arrow"]) > 0:
        if arrow_loading_time < 1:
            arrow_loaded = False
            arrow_loading_time = arrow_loading_time + dt
        else:
            arrow_loaded = True
    else:
        arrow_loaded = False
    is_down = base.mouseWatcherNode.is_button_down
    if player.player["combat type"] == "archery":
        if is_down("mouse1") and arrow_loaded:
            draw_sec = draw_sec + dt
        elif draw_sec > 0:
            archery.shooting(world, cam_anchor, draw_sec)
            arrow_loading_time = 0
            draw_sec = 0
    return task.cont

def arrow_flying_task(task):
    archery.arrow_flying(world)
    return task.cont

def debug_toggle():
    global is_debug_show
    if is_debug_show:
        debugNP.hide()
        is_debug_show = False
    else:
        debugNP.show()
        is_debug_show = True        

def toggle_main_gui(pause = False):
    if gui.main_gui_show == False and pause == True:
        pause_on()
        gui.show_main_gui(props, set_win_props)
    elif gui.main_gui_show == False and pause == False:
        gui.show_main_gui(props, set_win_props)
    else:
        gui.hide_main_gui(props, set_win_props, pause_off)

def pause_on():
    global is_pause
    if is_pause == False:
        taskMgr.remove("game time task")
        taskMgr.remove("physics task")
        taskMgr.remove("walk task")
        taskMgr.remove("skybox task")
        taskMgr.remove("sun task")
        taskMgr.remove("draw bow task")
        taskMgr.remove("arrow flying task")
        is_pause = True
        
def pause_off():
    global is_pause
    if is_pause == True:
        taskMgr.add(jdatetime.game_epoch_task, "game time task")
        taskMgr.add(physics_task, "physics task", sort=1)
        taskMgr.add(walk_task, "walk task", sort=3)
        taskMgr.add(skybox_task, "skybox task", sort=5)
        taskMgr.add(sun_task, "sun task", sort=5)
        taskMgr.add(draw_bow, "draw bow task", sort=5)
        taskMgr.add(arrow_flying_task, "arrow flying task", sort=6)
        is_pause = False

def delete_save_file():
    if os.path.isfile("saves/save_data.pkl"):
        os.remove("saves/save_data.pkl")
    no_save_file = open("saves/no_save_data", "w")
    no_save_file.write("There is no saved data currently.")
    no_save_file.close()

def hand_event(task):
    handevents.hand_event(cam_anchor, world)
    return(task.cont)
        
base = ShowBase()
#base.bufferViewer.toggleEnable()
render.setAntialias(AntialiasAttrib.MMultisample)
pipeline = simplepbr.init()
pipeline.enable_shadows = True
pipeline.use_normal_maps = True
pipeline.exposure = 1.0
rhodopsin = math.log(3.3)
base.setFrameRateMeter(True)
props = WindowProperties()
props.setMouseMode(WindowProperties.M_relative)
props.setCursorHidden(True)
win_x_o = 1200
win_y_o = 800
fullscreen_status = False
set_win_props()
world = BulletWorld()
world.setGravity(Vec3(0, 0, -9.81))
debugNode = BulletDebugNode('Debug')
debugNode.showWireframe(True)
debugNode.showConstraints(True)
debugNode.showBoundingBoxes(True)
debugNode.showNormals(True)
debugNP = render.attachNewNode(debugNode)
debugNP.show()
is_debug_show = True
world.setDebugNode(debugNP.node())
world.setGroupCollisionFlag(0, 1, True)
world.setGroupCollisionFlag(0, 2, True)
walk_speed = 12.5
skybox = loader.load_model("./models/skybox.gltf")
#skybox.set_light_off()
skybox.reparent_to(render)
sun_anchor = render.attach_new_node("sun anchor")
sun = DirectionalLight('sun')
sun.setColorTemperature(6500)
sun.get_lens().set_near_far(3400-512, 3400+512)
sun.get_lens().set_film_size(2048,2048)
sun.show_frustum()
sun.setShadowCaster(True, 16384, 16384)
sun_np = render.attachNewNode(sun)
render.setLight(sun_np)
sun_set_color = 1
alight = AmbientLight('alight')
alight.setColor((1.0, 1.0, 1.0, 1.0))
alight_triggered = False
alnp = render.attachNewNode(alight)
render.setLight(alnp)
pupil_dia = 0.008
skybox.set_light(alnp)
cam_anchor = render.attach_new_node("cam anchor")
cam_dist_end = render.attach_new_node("cam dist end")
cam_dist_start = render.attach_new_node("cam dist start")
lens = PerspectiveLens()
lens.setFilmSize(36, 24)
lens.setFocalLength(50)
lens.set_near(0.3)
lens.set_far(3800)
base.cam.node().set_lens(lens)
is_pause = False
gui.create_gui_elements(props, set_win_props, pause_off)
base.accept("m", toggle_main_gui)
base.accept("escape", toggle_main_gui, [True])
forward_button =  KeyboardButton.ascii_key('w')
backward_button = KeyboardButton.ascii_key("s")
left_button = KeyboardButton.ascii_key("a")
right_button = KeyboardButton.ascii_key("d")
base.accept("0", combat_type_switch, ["0"])
base.accept("1", combat_type_switch, ["1"])
base.accept("2", combat_type_switch, ["2"])
base.accept("e", handevents.do_hand_event)
base.accept("f12", debug_toggle)
base.accept("control-f12", archery.buy_arrows, [30])
base.accept("alt-enter", toggle_fullscreen)
draw_sec = 0
arrow_loading_time = 0
player.player_bp(world)
ply_nd = player.player["ply_nd"]
ply_bp_np = player.player["ply_bp_np"]
ply_bp_np.set_pos(load_data["Player pos"])
is_pause = False
load_addons(world)
taskMgr.add(jdatetime.game_epoch_task, "game time task")
taskMgr.add(physics_task, "physics task", sort=1)
taskMgr.add(mouse_task, "mouse task", sort=2)
taskMgr.add(walk_task, "walk task", sort=3)
taskMgr.add(cam_task, "cam task", sort=4)
taskMgr.add(skybox_task, "skybox task", sort=5)
taskMgr.add(sun_task, "sun task", sort=5)
taskMgr.add(eye_exposure, "eye task", sort=6)
taskMgr.add(draw_bow, "draw bow task", sort=5)
taskMgr.add(arrow_flying_task, "arrow flying task", sort=6)
taskMgr.add(hand_event, "hand event task", sort=6)

delete_save_file()
base.run()
