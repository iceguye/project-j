#    Copyright © 2017-2020 IceGuye.
#
#    This program is free software: you can redistribute it and/or
#    modify it under the terms of the GNU General Public License as
#    published by the Free Software Foundation, version 3 of the
#    License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see
#    <http://www.gnu.org/licenses/>.

from panda3d.core import *
from panda3d.bullet import *
from direct.actor.Actor import Actor
from loadgame import load_data

def player_bp(world):
    radius = 0.25
    height = 1.8
    global player
    shape = BulletCapsuleShape(radius, height - 2 * radius, ZUp)
    ply_nd = BulletCharacterControllerNode(shape, 0.4, 'Player')
    player["ply_nd"] = ply_nd
    ply_nd.set_gravity(9.81)
    ply_bp_np = render.attach_new_node(ply_nd)
    player["ply_bp_np"] = ply_bp_np
    ply_bp_np.set_collide_mask(BitMask32.bit(1))
    world.attach_character(ply_bp_np.node())

player = {"ply_bp_np": None,
          "ply_nd": None,
          "combat type": load_data["Combat type"],}
