#    Copyright © 2017-2020 IceGuye.
#
#    This program is free software: you can redistribute it and/or
#    modify it under the terms of the GNU General Public License as
#    published by the Free Software Foundation, version 3 of the
#    License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see
#    <http://www.gnu.org/licenses/>.

import os

for filename in os.listdir("saves"):
    filepath = "saves/" + filename
    os.remove(filepath)

no_save_data = open("saves/no_save_data", "w")
no_save_data.write("There is no saved data currently.")
no_save_data.close()
