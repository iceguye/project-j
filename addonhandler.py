from os import listdir
import importlib

def load_addons(world):
    for i in listdir("addons"):
        module_path = "addons." + i
        globals()[i] = importlib.import_module(module_path)
        scene = getattr(globals()[i], "scene")
        scene.scene_load(world)
        actor_dict.update(scene.actor_dict)     

actor_dict = {}
