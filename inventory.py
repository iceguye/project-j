#    Copyright © 2017-2020 IceGuye.
#
#    This program is free software: you can redistribute it and/or
#    modify it under the terms of the GNU General Public License as
#    published by the Free Software Foundation, version 3 of the
#    License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see
#    <http://www.gnu.org/licenses/>.


from loadgame import load_data

def add_stuff(name, number, attr):
    global inventory
    i = 0
    while(i<number):
        inventory[name].append(attr)
        i = i + 1

def remove_stuff(name, number):
    global inventory
    i = 0
    while(i<number):
        inventory[name].pop(0)
        i = i + 1
    
inventory = load_data["Inventory"]
