/*
    Copyright © 2017-2021 IceGuye.

    This program is free software: you can redistribute it and/or
    modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation, version 3 of the
    License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <http://www.gnu.org/licenses/>.
*/

#include <Python.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

double retainability(double interval) {
  double result;
  interval = interval / 60.0;
  if (interval >= 1.0) {
    result = 1.84 / (pow(log10(interval), 1.25) + 1.84);
  } else {
    result = 1.0;
  }
  return result;
}

static PyObject *py_time = NULL;

static PyObject *py_time_set_callback(PyObject *dummy, PyObject *args){
  PyObject *result = NULL;
  PyObject *temp;
  if (PyArg_ParseTuple(args, "O:set_callback", &temp)) {
    if(!PyCallable_Check(temp)) {
      PyErr_SetString(PyExc_TypeError, "parameter must be callable");
      return NULL;
    }
    Py_XINCREF(temp);
    Py_XDECREF(py_time);
    py_time = temp;
    Py_XINCREF(Py_None);
    result = Py_None;
  }
  return result;
}

static PyObject *forgetting(PyObject *self, PyObject *args){
  PyObject *archery_list;
  PyObject *new_archery_list;
  unsigned long long int seed;
  double myrand;
  Py_ssize_t list_len;
  PyObject *old_list_item;
  PyObject *shooting_time_obj;
  double shooting_time;
  PyObject *current_time_obj;
  double current_time;
  struct timespec spec;
  double interval;
  double retain;
  
  if (!PyArg_ParseTuple(args, "OL", &archery_list, &seed)) {
    return NULL;
  }
  srand(seed);
  new_archery_list = PyList_New(0);
  list_len = PyList_Size(archery_list);
  current_time_obj = PyObject_CallObject(py_time, NULL);
  current_time = PyFloat_AsDouble(current_time_obj);
  Py_ssize_t i;
  for (i=0; i < list_len; i=i+1) {
    old_list_item = PyList_GetItem(archery_list, i);
    shooting_time_obj = PyDict_GetItemString(old_list_item, "shoot time");
    shooting_time = PyFloat_AsDouble(shooting_time_obj);
    interval = current_time - shooting_time;
    retain = retainability(interval);
    myrand = (double)(rand() % 101) / 100.0;
    if (myrand < retain) {
      PyList_Append(new_archery_list, old_list_item);
    }
  }
  return new_archery_list;
}

static PyObject *avg_draw_sec(PyObject *self, PyObject *args) {
  PyObject *archery_list;
  Py_ssize_t list_len;
  PyObject *list_item;
  PyObject *draw_time_obj;
  double draw_time;
  double sum_sec;
  double avg_sec;
  if (!PyArg_ParseTuple(args, "O", &archery_list)) {
    return NULL;
  }
  list_len = PyList_Size(archery_list);
  Py_ssize_t i;
  sum_sec = 0.0;
  for (i=0; i<list_len; i=i+1) {
    list_item = PyList_GetItem(archery_list, i);
    draw_time_obj = PyDict_GetItemString(list_item, "draw time");
    draw_time = PyFloat_AsDouble(draw_time_obj);
    sum_sec = sum_sec + draw_time;
  }
  avg_sec = sum_sec / (double)list_len;
  return PyFloat_FromDouble(avg_sec);
}

static PyMethodDef ArcherylistMethods[] =
  {
    {"py_time_set_callback",
     py_time_set_callback,
     METH_VARARGS,
     "Set the Python standard time function to be able to callback in C."},
    {"forgetting",
     forgetting,
     METH_VARARGS,
     "Forgetting calculation in C function"},
    {"avg_draw_sec",
     avg_draw_sec,
     METH_VARARGS,
     "Average draw time calculation in C function"},
    {NULL, NULL, 0, NULL}
  };

static struct PyModuleDef archerylistmodule =
  {
    PyModuleDef_HEAD_INIT,
    "archerylist",
    "Archery list C lib functions",
    -1,
    ArcherylistMethods
  };

PyMODINIT_FUNC PyInit_archerylist(void) {
  return PyModule_Create(&archerylistmodule);
}
