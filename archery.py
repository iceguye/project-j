#    Copyright © 2017-2021 IceGuye.
#
#    This program is free software: you can redistribute it and/or
#    modify it under the terms of the GNU General Public License as
#    published by the Free Software Foundation, version 3 of the
#    License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see
#    <http://www.gnu.org/licenses/>.

import math
from statistics import mean
from morerandom import rand as random
from morerandom import get_current_seed, get_id
from panda3d.core import *
from panda3d.bullet import *
from loadgame import load_data
from inventory import inventory, remove_stuff
from archerylist import forgetting, avg_draw_sec, py_time_set_callback
from time import time
import json

def inch_meter(inch):
    meter = inch * 0.0254
    return(meter)

def meter_inch(meter):
    inch = meter / 0.0254
    return(meter)

def ms_fps(ms):
    fps = ms * 3.28084
    return(fps)

def fps_ms(fps):
    ms = fps / 3.28084
    return(fps)

def lbs_newton(lbs):
    newton = lbs / 4.4482216153
    return(newton)

def get_dist(p1, p2):
    pow_x = (p1.get_x() - p2.get_x()) ** 2
    pow_y = (p1.get_y() - p2.get_y()) ** 2
    pow_z = (p1.get_z() - p2.get_z()) ** 2
    dist = math.sqrt(pow_x + pow_y + pow_z)
    return(dist)

def rand_err():
    if random() > 0.5:
        err_dir = 1
    else:
        err_dir = -1
    rand_err = err_dir * 0.38 * random()
    return(rand_err)

def rand_plus_minus():
    if random() > 0.5:
        err_dir = 1
    else:
        err_dir = -1
    return(err_dir)

def after_hit(world, arrow_shot, hitresult):
    global hit_barrier_sound
    hitnode = hitresult.get_node()
    arrow_shot[1] = Vec3(0, 0, 0)
    arrow_shot[3] = 0
    if str(hitnode).find("Barrier") != -1 or str(hitnode).find("Target") != -1:
        if hit_barrier_sound == None:
            hit_barrier_sound = loader.load_sfx("sounds/arrow-hit-barrier.flac")
        hit_barrier_sound.play()
    hit_nodepath = base.render.find_path_to(hitnode)
    current_pos = arrow_shot[0].get_pos()
    current_hpr = arrow_shot[0].get_hpr()
    arrow_shot[0].reparent_to(hit_nodepath)
    arrow_shot[0].set_pos(render, current_pos)
    arrow_shot[0].set_hpr(render, current_hpr)
    arrow_ghost_np = render.attach_new_node(arrow_shot[4])
    arrow_ghost_np.reparent_to(arrow_shot[0])
    arrow_ghost_np.set_collide_mask(BitMask32.bit(2))
    last_arrow = len(arrows_shot) - 1
    if str(hitnode).find("Target") == -1:
        status_num = arrows_shot[last_arrow][5]["Status"]
        arrows_shot[last_arrow][5]["Status"] = status_num - random() - 0.2
    if arrows_shot[last_arrow][5]["Status"] <= 0.0:
        arrows_shot[last_arrow][0].remove_node()
        arrows_shot[last_arrow][2].remove_node()
        world.remove_ghost(arrows_shot[last_arrow][4])
        arrows_shot.pop(last_arrow)

def after_penetrated_hit(world, arrow_shot, penetrated):
    arrow = arrow_shot[0]
    pnt_pos = penetrated.get_hit_pos()
    arrow_pos = arrow.get_pos()
    pnt_len = get_dist(arrow_pos, pnt_pos)
    adjust_y = pnt_len + 0.1 + 0.7 * random()
    arrow.set_y(arrow, adjust_y)
    hitresult = penetrated
    after_hit(world, arrow_shot, hitresult)

def shooting(world, cam_anchor, draw_sec):
    global shooting_sound
    global draw_sec_list
    seed = get_current_seed()
    py_time_set_callback(time)
    draw_sec_list = forgetting(draw_sec_list, seed)
    #if len(draw_sec_list) >= 65532:
    #    draw_sec_list.pop(0)
    # Do not set upper limit of the list until further research.
    draw_sec_list.append({"draw time":draw_sec, "shoot time":time()})
    avg_draw = avg_draw_sec(draw_sec_list)
    inaccuracy = (draw_sec - avg_draw) / avg_draw
    if inaccuracy >= 1:
        inaccuracy = 0.9999
    elif inaccuracy <= -1:
        inaccuracy = -0.9999
    fatigue = 0
    horizontal_error = 0.0
    vertical_error = 0.0
    draw_len = inch_meter(30)
    if inaccuracy > 0:
        fatigue = inaccuracy
        drew  = draw_len - brace_h
        drew = drew - drew * fatigue
        draw_len = drew + brace_h
        horizontal_error = rand_plus_minus() * 3.4 * fatigue + rand_err()
        vertical_error = rand_plus_minus() * 3.4 * fatigue + rand_err()
    else:
        drew  = draw_len - brace_h
        drew = drew + drew * inaccuracy
        draw_len = drew + brace_h
        horizontal_error = 3.4 * (-inaccuracy) + rand_err()
        vertical_error = rand_err()
    energy_stored = 0.5 * k_factor * math.pow(draw_len, 2)
    energy_left = 0.5 * k_factor * math.pow(brace_h, 2)
    energy_released = energy_stored - energy_left
    end_spd_pow = 2 * energy_released / arrow_weight
    end_spd = math.sqrt(end_spd_pow)
    arrow = loader.load_model("./models/arrow.gltf")
    arrow_ghost_box = arrow.find("**/arrow-ghost-box")
    arrow = arrow.find("**/arrow")
    arrow.reparent_to(render)
    arrow.set_pos(cam_anchor.get_pos())
    arrow.set_x(cam_anchor, -0.25)
    arrow.set_y(cam_anchor, -0.5)
    arrow.set_h(cam_anchor.get_h())
    arrow.set_p(cam_anchor.get_p())
    arrow.set_h(arrow.get_h() - horizontal_error)
    arrow.set_p(arrow.get_p() + vertical_error)
    arrow_tip = render.attach_new_node("Arrow Tip")
    arrow_tip.reparent_to(arrow)
    arrow_tip.set_y(arrow, 0.85)
    spd = - (arrow_tip.get_pos(base.render) - arrow.get_pos())
    spd.normalize()
    spd = spd * end_spd
    spd_v = 0
    mesh = BulletTriangleMesh()
    geom_node = arrow_ghost_box.get_child(0).node()
    for i in range(geom_node.get_num_geoms()):
        geom = geom_node.get_geom(i)
        mesh.add_geom(geom)
    shape = BulletTriangleMeshShape(mesh, dynamic=False)
    arrow_id = inventory["Arrow"][0]["ID"]
    node_name = json.dumps({"ID": arrow_id,
                            "Name": "Arrow",
                            "Interaction": "Pick"})
    arrow_ghost_nd = BulletGhostNode(node_name)
    arrow_ghost_nd.add_shape(shape)
    world.attach_ghost(arrow_ghost_nd)
    if shooting_sound == None:
        shooting_sound = loader.load_sfx("sounds/archery-shooting.flac")
    shooting_sound.play()
    arrow_attr = inventory["Arrow"][0]
    arrows_shot.append([arrow,
                        spd,
                        arrow_tip,
                        spd_v,
                        arrow_ghost_nd,
                        arrow_attr])
    remove_stuff("Arrow", 1)
    last_shooting_time = time()

def arrow_flying(world):
    global hit_barrier_sound
    dt = globalClock.get_dt()
    for arrow_shot in arrows_shot:
        if arrow_shot[1] != Vec3(0,0,0):
            arrow = arrow_shot[0]
            spd = arrow_shot[1]
            arrow_tip = arrow_shot[2]
            spd_v = arrow_shot[3] + (-9.81) * dt
            old_pos = arrow.get_pos()
            old_vec = arrow_tip.get_pos(base.render) - old_pos
            old_vec.normalize()
            arrow.set_pos(arrow.get_pos() + spd*dt)
            arrow.set_z(arrow.get_z() + spd_v*dt)
            new_pos = arrow.get_pos()
            new_vec = new_pos - old_pos
            new_vec.normalize()
            delta_angle = new_vec.angle_deg(old_vec) - 180
            arrow.set_p(arrow.get_p()-delta_angle)
            arrow_shot[0] = arrow
            arrow_shot[2] = arrow_tip
            arrow_shot[3] = spd_v
            mask = BitMask32.bit(0)
            p_from = old_pos
            p_to = new_pos
            penetrated = world.ray_test_closest(p_from, p_to, mask)
            p_from = new_pos
            p_to = new_pos + new_vec * 0.85
            hitresult = world.ray_test_closest(p_from, p_to, mask)
            if penetrated.has_hit():
                after_penetrated_hit(world, arrow_shot, penetrated)
            elif hitresult.has_hit():
                after_hit(world, arrow_shot, hitresult)
                    
def buy_arrows(how_many):
    i = 0
    while(i<how_many):
        inventory["Arrow"].append({"ID": get_id(),
                                   "Status": 1.0})

std_draw_len = inch_meter(28)
std_draw_weight = lbs_newton(55)
k_factor = std_draw_weight / std_draw_len
arrow_weight = 0.0281875
brace_h = inch_meter(7.5)
draw_sec_list = load_data["Archery training data"]
arrows_shot = []
shooting_sound = None
hit_barrier_sound = None
