#    Copyright © 2017-2020 IceGuye.
#
#    This program is free software: you can redistribute it and/or
#    modify it under the terms of the GNU General Public License as
#    published by the Free Software Foundation, version 3 of the
#    License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see
#    <http://www.gnu.org/licenses/>.

import os
import pickle
from panda3d.core import *
from time import time
from morerandom import get_id

i = 0
arrow_initial = 30
arrow_tmp_list = []
while(i<arrow_initial):
    arrow_tmp_list.append({"ID": get_id(),
                           "Status": 1.0})
    i = i + 1

inventory = {"Arrow": arrow_tmp_list,
             "Water Flask": [{"ID": get_id(),
                              "Status": 0.0}],}

if os.path.isfile("saves/save_data.pkl"):
    save_file = open("saves/save_data.pkl", "rb")
    load_data = pickle.load(save_file)
    save_file.close()
else:
    load_data = {"Game epoch": 0.0,
                 "Player pos": LPoint3(10467.8, 7592.87, 200),
                 "Inventory": inventory,
                 "Archery training data": [{"draw time":2.0,
                                            "shoot time": time()}],
                 "Combat type": None,
                 "Epoch year": 193,
                 "Epoch month": 12,
                 "Epoch date": 14,
                 "Epoch hours": 17,
                 "Epoch minutes": 20,
                 "Epoch seconds": 0,
                 "Epoch day": 7,
                 "More to add...": "More to add...",
    }
