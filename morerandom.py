#    Copyright © 2017-2020 IceGuye.
#
#    This program is free software: you can redistribute it and/or
#    modify it under the terms of the GNU General Public License as
#    published by the Free Software Foundation, version 3 of the
#    License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see
#    <http://www.gnu.org/licenses/>.

# WARNING: this library is NOT for security and secret purpose. It
# just makes the gameplay to be slightly more unpredictable.

import os
import time
import random
import base64

def rand():
    random.seed(token_num + time.time())
    rand_result = random.random()
    return(rand_result)

def get_current_token():
    return token_num

def get_current_seed():
    seed = get_current_token() + int(time.time()*1000)
    seed = seed % 999999999999
    return seed

def get_new_token():
    global token
    global token_num
    token = os.urandom(32)
    token_num = int.from_bytes(token, 'little')
    token_num = token_num % 999999999999
    return token_num

def update_token(hours):
    global token
    global token_num
    if hours == 23:
        get_new_token()

def get_id():
    id_base64 = base64.b64encode(os.urandom(32))
    id_str = id_base64.decode("utf-8")
    return(id_str)

token = os.urandom(32)
token_num = int.from_bytes(token, 'little')
token_num = token_num % 999999999999
