#    Copyright © 2017-2021 IceGuye.
#
#    This program is free software: you can redistribute it and/or
#    modify it under the terms of the GNU General Public License as
#    published by the Free Software Foundation, version 3 of the
#    License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see
#    <http://www.gnu.org/licenses/>.

from panda3d.core import *
from panda3d.bullet import *
from direct.actor.Actor import Actor
import json
from morerandom import get_id

def scene_load(world):
    static_no_bp()
    static_bp(world)
    player_house_door(world)
    wells(world)
    target_butts(world)

def static_no_bp():
    model = scene.find("**/static_scane_non_barrier")
    model.reparent_to(render)
    
def static_bp(world):
    barriers = scene.find_all_matches("**/*_barrier")
    for barrier in barriers:
        barrier.reparent_to(render)
        barrier.set_depth_offset(-2)
        mesh = BulletTriangleMesh()
        geom_node = barrier.get_child(0).node()
        for i in range(geom_node.get_num_geoms()):
            geom = geom_node.get_geom(i)
            mesh.add_geom(geom)
        shape = BulletTriangleMeshShape(mesh, dynamic=False)
        barrier_nd = BulletRigidBodyNode('Barrier node')
        barrier_nd.add_shape(shape)
        barrier_np = render.attach_new_node(barrier_nd)
        barrier_np.reparent_to(barrier)
        barrier_np.set_collide_mask(BitMask32.bit(0))
        world.attach_rigid_body(barrier_nd)

def target_butts(world):
    butts = scene.find_all_matches("**/target-butt-*")
    for butt in butts:
        butt.reparent_to(render)
        butt.set_depth_offset(-2)
        mesh = BulletTriangleMesh()
        geom_node = butt.get_child(0).node()
        for i in range(geom_node.get_num_geoms()):
            geom = geom_node.get_geom(i)
            mesh.add_geom(geom)
        shape = BulletTriangleMeshShape(mesh, dynamic=False)
        barrier_nd = BulletRigidBodyNode('Target node')
        barrier_nd.add_shape(shape)
        barrier_np = render.attach_new_node(barrier_nd)
        barrier_np.reparent_to(butt)
        barrier_np.set_collide_mask(BitMask32.bit(0))
        world.attach_rigid_body(barrier_nd)

def player_house_door(world):
    model = scene.find("**/player_house_door_character")
    rigid_model = model.find("**/player_house_door_rigid")
    rigid_model.hide()
    actor = Actor(model)
    actor.reparent_to(render)
    mesh = BulletTriangleMesh()
    geom_node = rigid_model.get_child(0).node()
    for i in range(geom_node.get_num_geoms()):
        geom = geom_node.get_geom(i)
        mesh.add_geom(geom)
    shape = BulletTriangleMeshShape(mesh, dynamic=False)
    door_nd = BulletRigidBodyNode('Door Barrier node')
    door_nd.add_shape(shape)
    door_np = render.attach_new_node(door_nd)
    door_np.set_collide_mask(BitMask32.bit(0))
    doorbone = actor.exposeJoint(None,"modelRoot","doorbone_h")
    door_np.node().set_kinematic(True)
    door_np.reparent_to(doorbone)
    door_np.set_pos(render, model.get_pos())
    door_np.set_hpr(render, (0,0,0))
    world.attach_rigid_body(door_nd)

    node_name = json.dumps({"ID": get_id(),
                            "Name": "Player's House Door",
                            "Interaction": "Open"})
    ghost_nd = BulletGhostNode(node_name)
    ghost_nd.add_shape(shape)
    ghost_np = render.attach_new_node(ghost_nd)
    ghost_np.set_collide_mask(BitMask32.bit(2))
    ghost_np.node().set_kinematic(True)
    ghost_np.reparent_to(doorbone)
    ghost_np.set_pos(render, model.get_pos())
    ghost_np.set_hpr(render, (0,0,0))
    world.attach_ghost(ghost_nd)
    actor_dict.update({
        str(ghost_nd): {
            "Actor": actor,
        },
    })

def wells(world):
    well_list = scene.find_all_matches("**/well-model-*")
    for well in well_list:
        well.reparent_to(render)
        mesh = BulletTriangleMesh()
        geom_node = well.get_child(0).node()
        for i in range(geom_node.get_num_geoms()):
            geom = geom_node.get_geom(i)
            mesh.add_geom(geom)
        shape = BulletTriangleMeshShape(mesh, dynamic=False)
        barrier_nd = BulletRigidBodyNode('Barrier node')
        barrier_nd.add_shape(shape)
        barrier_np = render.attach_new_node(barrier_nd)
        barrier_np.reparent_to(well)
        barrier_np.set_collide_mask(BitMask32.bit(0))
        world.attach_rigid_body(barrier_nd)
        
        node_name = json.dumps({"ID": get_id(),
                                "Name": "Well",
                                "Interaction": "Activate"})
        ghost_nd = BulletGhostNode(node_name)
        ghost_nd.add_shape(shape)
        ghost_np = render.attach_new_node(ghost_nd)
        ghost_np.reparent_to(well)
        ghost_np.set_collide_mask(BitMask32.bit(2))
        world.attach_ghost(ghost_nd)
    
addon_path = "addons/default/"
scene_path = addon_path + "models/scene.gltf"
scene = loader.load_model(scene_path)

# Actor dict:
actor_dict= {}
