#    Copyright © 2017-2020 IceGuye.
#
#    This program is free software: you can redistribute it and/or
#    modify it under the terms of the GNU General Public License as
#    published by the Free Software Foundation, version 3 of the
#    License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see
#    <http://www.gnu.org/licenses/>.

from panda3d.core import *
from panda3d.bullet import *
import gui
from addonhandler import actor_dict
from inventory import add_stuff, inventory
from archery import arrows_shot
import json

def hand_event(cam_anchor, world):
    global hand_event_data
    global hand_touch_node
    global hand_event_node
    if hand_event_node == None:
        hand_event_node = render.attach_new_node("hand event")
    hand_event_node.set_pos(cam_anchor.get_pos())
    hand_event_node.set_y(cam_anchor, -3)
    hand_event_node.set_x(cam_anchor, -0.25)
    hand_event_node.set_hpr(cam_anchor.get_hpr())
    p_from = cam_anchor.get_pos()
    p_to = hand_event_node.get_pos()
    ts_from = TransformState.make_pos(p_from)
    ts_to = TransformState.make_pos(p_to)
    shape = BulletSphereShape(0.5)
    mask = BitMask32.bit(2)
    hand_trigger = world.sweep_test_closest(shape, ts_from, ts_to, mask)
    hand_touch_node = hand_trigger.get_node()
    string_finding = str(hand_touch_node)
    start_cut = string_finding.find('{"ID": ')
    end_cut = string_finding.find("}") + len("}")
    data_str = string_finding[start_cut:end_cut]
    if len(data_str) > 0:
        data_dict = json.loads(data_str)
    else:
        data_dict = {"ID": "",
                     "Name": "",
                     "Interaction": ""}
    if str(hand_touch_node) not in hand_event_data:
        hand_event_data.update({
            str(hand_touch_node): data_dict,
        })
    name = hand_event_data[str(hand_touch_node)]["Name"]
    interaction = hand_event_data[str(hand_touch_node)]["Interaction"]
    pop_up_text = gui.hand_event_text(name, interaction)

def do_hand_event():
    global hand_touch_node
    global hand_event_data
    item_id = hand_event_data[str(hand_touch_node)]["ID"]
    name =  hand_event_data[str(hand_touch_node)]["Name"]
    interaction = hand_event_data[str(hand_touch_node)]["Interaction"]
    if interaction == "Pick" and name == "Arrow":
        hand_touch_np = base.render.find_path_to(hand_touch_node)
        parent_model = hand_touch_np.get_parent()
        i = 0
        while(i<len(arrows_shot)):
            arrow = arrows_shot[i]
            check_id = arrow[5]["ID"]
            if check_id == item_id:
                arrow_attr = arrow[5]
                break
            else:
                i = i + 1
        arrows_shot.pop(i)
        add_stuff(name, 1, arrow_attr)
        hand_touch_np.remove_node()
        parent_model.remove_node()
        del hand_event_data[str(hand_touch_node)]
    elif interaction == "Open":
        actor = actor_dict[str(hand_touch_node)]["Actor"]
        actor.set_play_rate(1.0, "open")
        actor.play("open")
        hand_event_data[str(hand_touch_node)]["Interaction"] = "Close"
    elif interaction == "Close":
        actor = actor_dict[str(hand_touch_node)]["Actor"]
        actor.set_play_rate(-1.0, "open")
        actor.play("open")
        hand_event_data[str(hand_touch_node)]["Interaction"] = "Open"

hand_touch_node = None
hand_event_data = {}
hand_event_node = None
