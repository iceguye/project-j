#    Copyright © 2017-2020 IceGuye.
#
#    This program is free software: you can redistribute it and/or
#    modify it under the terms of the GNU General Public License as
#    published by the Free Software Foundation, version 3 of the
#    License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see
#    <http://www.gnu.org/licenses/>.

from direct.gui.DirectGui import *
from direct.gui.OnscreenText import OnscreenText
import pickle
import os
from player import player
import archery
import jdatetime
from inventory import inventory

def exit_game():
    really_exit_t.show()
    really_exit_b.show()
    not_really_exit_b.show()
    save_and_exit_b.hide()
    exit_b.hide()

def save_exit_game():
    really_save_t.show()
    really_save_b.show()
    not_really_save_b.show()
    save_and_exit_b.hide()
    exit_b.hide()

def save_record():
    save_data = {"Game epoch": jdatetime.game_epoch,
                 "Player pos": player["ply_bp_np"].get_pos(),
                 "Inventory": inventory,
                 "Archery training data": archery.draw_sec_list,
                 "Combat type": player["combat type"],
                 "Epoch year": jdatetime.game_epoch_year,
                 "Epoch month": jdatetime.game_epoch_month,
                 "Epoch date": jdatetime.game_epoch_date,
                 "Epoch hours": jdatetime.game_epoch_hours,
                 "Epoch minutes": jdatetime.game_epoch_minutes,
                 "Epoch seconds": jdatetime.game_epoch_seconds,
                 "Epoch day": jdatetime.game_epoch_day,
                 "More to add...": "More to add...",
    }
    return(save_data)
    
def really_save():
    save_data = save_record()
    if os.path.isfile("saves/no_save_data"):
        os.remove("saves/no_save_data")
    save_file = open("saves/save_data.pkl", "wb")
    pickle.dump(save_data, save_file, pickle.HIGHEST_PROTOCOL)
    save_file.close()
    print("Be seeing you...")
    os._exit(1)

def show_main_gui(props, set_win_props):
    global main_gui_show
    main_gui_show = True
    save_and_exit_b.show()
    exit_b.show()
    props.setCursorHidden(False)
    props.setMouseMode(props.M_absolute)
    set_win_props()
        
def hide_main_gui(props, set_win_props, pause_off):
    global main_gui_show
    main_gui_show = False
    pause_off()
    really_save_t.hide()
    really_exit_t.hide()
    really_exit_b.hide()
    not_really_exit_b.hide()
    really_save_b.hide()
    not_really_save_b.hide()
    save_and_exit_b.hide()
    exit_b.hide()
    props.setCursorHidden(True)
    props.setMouseMode(props.M_relative)
    set_win_props()

def hand_event_text(name, interaction):
    if interaction != "":
        hand_event_t.show()
        hand_event_t["text"] = "E: " + interaction
        hand_event_name_t.show()
        hand_event_name_t["text"] = name
    else:
        hand_event_t.hide()
        hand_event_name_t.hide()

def create_gui_elements(props, set_win_props, pause_off):
    global save_and_exit_b
    global really_save_b
    global not_really_save_b
    global exit_b
    global really_exit_b
    global not_really_exit_b
    global really_exit_t
    global really_save_t
    global hand_event_t
    global hand_event_name_t
    really_save_t = OnscreenText(text="Really save?",
                                 fg=(1,1,1,1),
                                 shadow=(0,0,0,0.6),
                                 pos=(0,0.1),
                                 scale=0.13)
    really_exit_t = OnscreenText(text="Really exit?",
                                 fg=(1,1,1,1),
                                 shadow=(0,0,0,0.6),
                                 pos=(0,0.1),
                                 scale=0.13)
    save_and_exit_b = DirectButton(text=("Save and Exit"),
                                   scale=.1,
                                   pos=(0, 0, 0.1),
                                   command=save_exit_game)
    exit_b = DirectButton(text=("Exit"),
                           scale=.1,
                           pos=(0, 0, -0.1),
                           command=exit_game)
    really_save_b = DirectButton(text=("Yes"),
                                 scale=.1,
                                 pos=(-0.5, 0, -0.1),
                                 command=really_save,)
    not_really_save_b = DirectButton(text=("No"),
                                     scale=0.1,
                                     pos=(0.5, 0, -0.1),
                                     command=hide_main_gui,
                                     extraArgs=[props,
                                                set_win_props,
                                                pause_off,])
    really_exit_b = DirectButton(text=("Yes"),
                                 scale=.1,
                                 pos=(-0.5, 0, -0.1),
                                 command=exit)
    not_really_exit_b = DirectButton(text=("No"),
                                     scale=0.1,
                                     pos=(0.5, 0, -0.1),
                                     command=hide_main_gui,
                                     extraArgs=[props,
                                                set_win_props,
                                                pause_off,])
    hand_event_t = OnscreenText(text="E: Pick",
                                 fg=(1,1,1,1),
                                 shadow=(0,0,0,0.6),
                                 pos=(0,0.26),
                                 scale=0.08)
    hand_event_name_t = OnscreenText(text="a name",
                                 fg=(1,1,1,1),
                                 shadow=(0,0,0,0.6),
                                 pos=(0,0.17),
                                 scale=0.08)
    hide_main_gui(props, set_win_props, pause_off)

main_gui_show = False
