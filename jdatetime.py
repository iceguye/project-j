#    Copyright © 2017-2020 IceGuye.
#
#    This program is free software: you can redistribute it and/or
#    modify it under the terms of the GNU General Public License as
#    published by the Free Software Foundation, version 3 of the
#    License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see
#    <http://www.gnu.org/licenses/>.

from time import time
from loadgame import load_data
from panda3d.core import *
import math
from morerandom import update_token

def game_epoch_task(task):
    global game_epoch
    global game_epoch_seconds
    global game_epoch_minutes
    global game_epoch_hours
    global game_epoch_day
    global game_epoch_date
    global game_epoch_month
    global game_epoch_year
    dt = globalClock.get_dt()
    game_epoch = game_epoch + dt * 20
    last_seconds = math.floor(game_epoch_seconds)
    game_epoch_seconds = game_epoch % 60
    this_seconds = math.floor(game_epoch_seconds)
    last_minutes = game_epoch_minutes
    if last_seconds != this_seconds and this_seconds == 0:
        game_epoch_minutes = game_epoch_minutes + 1
        game_epoch_minutes = game_epoch_minutes % 60
    this_minutes = game_epoch_minutes
    last_hours = game_epoch_hours
    if last_minutes != this_minutes and this_minutes == 0:
        game_epoch_hours = game_epoch_hours + 1
        game_epoch_hours = game_epoch_hours % 24
        update_token(game_epoch_hours)
    this_hours = game_epoch_hours
    last_date = game_epoch_date
    if last_hours != this_hours and this_hours == 0:
        game_epoch_day = game_epoch_day + 1
        if game_epoch_day > 7:
            game_epoch_day = 1
        game_epoch_date = game_epoch_date + 1
        if game_epoch_month in long_months:
            if game_epoch_date > 31:
                game_epoch_date = 1
        elif game_epoch_month in short_months:
            if game_epoch_date > 30:
                game_epoch_date = 1
        elif game_epoch_month == 2 and game_epoch_year % 4 != 0:
            if game_epoch_date > 28:
                game_epoch_date = 1
        elif game_epoch_month == 2 and game_epoch_year % 4 == 0:
            if game_epoch_date > 29:
                game_epoch_date = 1
    this_date = game_epoch_date
    last_month = game_epoch_month
    if last_date != this_date and this_date == 1:
        game_epoch_month = game_epoch_month + 1
        if game_epoch_month > 12:
            game_epoch_month = 1
    this_month = game_epoch_month
    if last_month != this_month and this_month == 1:
        game_epoch_year = game_epoch_year + 1
    return(task.cont)

game_epoch = load_data["Game epoch"]
game_epoch_year = load_data["Epoch year"]
game_epoch_month = load_data["Epoch month"]
game_epoch_date = load_data["Epoch date"]
game_epoch_hours = load_data["Epoch hours"]
game_epoch_minutes = load_data["Epoch minutes"]
game_epoch_seconds = load_data["Epoch seconds"]
long_months = [1, 3, 5, 7, 8, 10, 12]
short_months = [4, 6, 9, 11]
game_epoch_day = load_data["Epoch day"]
days_dict = {1: "Lūnae",
             2: "Mārtis",
             3: "Mercuriī",
             4: "Iovis",
             5: "Veneris",
             6: "Saturnī",
             7: "Sōlis"}
